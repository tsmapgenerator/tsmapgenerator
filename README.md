# TSMapGenerator

TSMapGenerator is a procedural C&C Tiberian Sun map generator using tiled based [Wave Function Collapse (WFC) algorithm](https://github.com/mxgmn/WaveFunctionCollapse).
The WFC algorithm implementation is provided by [DeBroglie](https://boristhebrave.github.io/DeBroglie/).

TSMapGenerator consists of three repositories:
1. [TSMap](https://gitlab.com/tsmapgenerator/tsmap) - Fork of [Starkku's MapTool](https://github.com/Starkku/MapTool) with some edits to support TSDeBroglie integration.
2. [TSDeBroglie](https://gitlab.com/tsmapgenerator/tsdebroglie) - Fork of [DeBroglie](https://github.com/BorisTheBrave/DeBroglie) with C&C Tiberian Sun map format support.
3. [TSMapGenerator](https://gitlab.com/tsmapgenerator/tsmapgenerator) - Provides a starting point to use TSMapGenerator and a script to run the generator.

This repository provides an example configuration for generating 64x64 sized maps with 4 starting points.

## Quick start
1. Make sure you have [.NET Framework 4.7.2 runtime](https://dotnet.microsoft.com/en-us/download/dotnet-framework/net472) installed.
2. Download the binary package from [here](https://gitlab.com/tsmapgenerator/tsmapgenerator/-/jobs/artifacts/master/download?job=deploy) and extract the package to any folder.
3. To generate a map with the example configuration, simply run `generate.bat` batch script. The generated map file will be named as `output.mpr`.

## Advanced (WIP)
To add new tiles or edit the default rules or constraints, please take a look into `tsmap.json`.
The rule configuration format is described in [DeBroglie's Command Line documentation](https://boristhebrave.github.io/DeBroglie/articles/config_files.html).

Remember to add new tiles also to `sample.mpr`.

### Tile format in TSDeBroglie
The format used to describe one tile contents in TS map is following:

`<isotile_id>_<subtile_id>_<height level>_<overlay_id>_<overlay_data>;<waypoint>|<terrain>`.

Note that `waypoint` and `terrain` sections are optional. By default we don't set any waypoint or terrain objects for any tile.

For examples see `tsmap.json`.

## Useful links
1. DeBroglie documentation - https://boristhebrave.github.io/DeBroglie/
2. The original Wave Function Collapse repository - https://github.com/mxgmn/WaveFunctionCollapse